package demo;
//求一組圖形中的最大面積
public class ShapeTest {
	
	public static void main(String[] args) {
		//new Shape();
		Shape[] shapes = new Shape[4];//創建Shape數組對象
		shapes[0] = new Circle(1);
		shapes[1] = new Circle(2);
		shapes[2] = new Square(1);
		shapes[3] = new Square(2);
		maxArea(shapes);
	}
	
	//求最大面積
	public static void maxArea(Shape[] shapes) {
		double max = shapes[0].area();//假設第一個圖形最大
		int maxIndex = 0;
		for(int i=1; i<shapes.length; i++) {
			double area = shapes[i].area();
			if(area>max) {
				max = area;
				maxIndex = i;
			}
		}
		System.out.println("最大面積為:"+max+",所在index為"+maxIndex);
	}
}

abstract class Shape{
//class Shape{
	protected double c;//周長
	//public double area();
	public abstract double area();
}

//class triangle extends Shape() {
abstract class triangle extends Shape {	
}

class Circle extends Shape{
	public Circle(double c) {
		this.c = c;
	}
	@Override
	public double area() {
		return 0.0796*c*c;
	}
}

class Square extends Shape{
	public Square(double c) {
		this.c = c;
	}
	@Override
	public double area() {
		return 0.0628*c*c;
	}
	
}